/*
 * Spiel.h
 *
 *  Created on: 06.12.2014
 *      Author: Alex
 */

#ifndef SPIEL_H_
#define SPIEL_H_

#include "Spieler.h"
#include "Spielbrett.h"

namespace connect_four{

/**
 ******************************************
 * @class Spiel
 * Class representing a connect four game.
 ******************************************
 */
class Spiel {
        std::unique_ptr<Spielbrett> sb;
        Spieler* spielerAmZug;
        bool gameFinished;
        unsigned limit;
    public:
        explicit Spiel(Spieler* sp1, Spieler* sp2);
        explicit Spiel(Spieler* sp1, Spieler* sp2,
              unsigned sbWidth, unsigned sbHeight, unsigned limit_);
        Spiel(const Spiel& other) = delete;
        Spiel(Spiel&& other) noexcept;
        ~Spiel() noexcept;
        Spiel& operator=(const Spiel& other) = delete;
        Spiel& operator=(Spiel&& other) noexcept;
        const Spielbrett& getSb() const;
        const Spieler& getCurrentPlayer() const;
        unsigned getLimit() const;
        bool isGameFinished() const;
        bool hasCurrentPlayerWon() const;
        bool isRemis() const;
        void nextPlayerMove(unsigned col);
        void toStartState();
};

} //namespace connect_four
#endif /* SPIEL_H_ */
