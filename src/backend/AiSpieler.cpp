/*
 * AiSpieler.cpp
 *
 *  Created on: 26.12.2014
 *      Author: Alex
 */

#include "AiSpieler.h"

#include "MinMaxAI.h"
#include "BewertungsFunktion.h"
#include "Spielbrett.h"

namespace connect_four{


AiSpieler::AiSpieler(const std::string& name_, FeldStatus spielsteine_,
                     std::unique_ptr<BewertungsFunktion> bf_, Difficulty difficulty) :
    Spieler(name_, spielsteine_), ai(std::unique_ptr<MinMaxAI>(new MinMaxAI(
                ((spielsteine_ == FeldStatus::SPIELER2) ? FeldStatus::SPIELER1 : FeldStatus::SPIELER2), 
				spielsteine_,
                std::move(bf_), 
				(difficulty == Difficulty::LEICHT) ? 3 : (difficulty == Difficulty::MITTEL) ? 5 : 8,
				(difficulty == Difficulty::LEICHT) ? 0.2 : (difficulty == Difficulty::MITTEL) ? 0.1 : 0.0))) {
}

void AiSpieler:: makeMove(Spielbrett& sb, unsigned limit, unsigned) const {
    unsigned col = ai->computeNextMove(sb, limit);
    sb.setFeld(col, sb.getFreeColumnIndex(col), spielsteine);
}


} //namespace connect_four
