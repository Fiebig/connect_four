/*
 * MenschlicherSpieler.cpp
 *
 *  Created on: 14.12.2014
 *      Author: Alex
 */

#include "MenschlicherSpieler.h"

#include "Spielbrett.h"

namespace connect_four{


MenschlicherSpieler::MenschlicherSpieler(const std::string& name_,
        FeldStatus spielsteine_) :
    Spieler(name_, spielsteine_) {
}

void MenschlicherSpieler:: makeMove(Spielbrett& sb, unsigned,
                                    unsigned col) const {
    if (sb.getFreeColumnIndex(col) != -1)
        sb.setFeld(col, sb.getFreeColumnIndex(col), spielsteine);
}


} //namespace connect_four
