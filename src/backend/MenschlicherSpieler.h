/*
 * MenschlicherSpieler.h
 *
 *  Created on: 06.12.2014
 *      Author: Alex
 */

#ifndef MENSCHLICHERSPIELER_H_
#define MENSCHLICHERSPIELER_H_

#include "Spieler.h"

namespace connect_four{


class MenschlicherSpieler final: public Spieler {
    public:
        MenschlicherSpieler(const std::string& name_, FeldStatus spielsteine_);

        void makeMove(Spielbrett& sb, unsigned, unsigned col) const override;
};

} //namespace connect_four
#endif /* MENSCHLICHERSPIELER_H_ */
