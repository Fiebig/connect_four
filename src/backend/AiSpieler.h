/*
 * AiSpieler.h
 *
 *  Created on: 06.12.2014
 *      Author: Alex
 */

#ifndef AISPIELER_H_
#define AISPIELER_H_

#include "Spieler.h"

namespace connect_four{


class MinMaxAI;
class BewertungsFunktion;

enum class Difficulty {
	LEICHT = 0, MITTEL = 1, SCHWER = 2
};

class AiSpieler final: public Spieler {
        std::unique_ptr<MinMaxAI> ai;
    public:
        AiSpieler(const std::string& name_, FeldStatus spielsteine_,
                  std::unique_ptr<BewertungsFunktion> bf_, Difficulty difficulty);

        void makeMove(Spielbrett& sb, unsigned limit, unsigned) const override;
};

} //namespace connect_four
#endif /* AISPIELER_H_ */
