/*
 * BewertungsFunktion.h
 *
 *  Created on: 25.12.2014
 *      Author: Alex
 */

#ifndef BEWERTUNGSFUNKTION_H_
#define BEWERTUNGSFUNKTION_H_

#include <memory>

namespace connect_four{


class Spielbrett;
enum class FeldStatus;

class BewertungsFunktion {
protected:
	virtual unsigned bewerteHorizontal(const Spielbrett&sp, FeldStatus fs,
			unsigned limit) const = 0;

	virtual unsigned bewerteVertikal(const Spielbrett&sp, FeldStatus fs,
			unsigned limit) const = 0;

	virtual unsigned bewerteDiagonal_BottumUp(const Spielbrett&sp,FeldStatus fs,
			unsigned limit) const = 0;

	virtual unsigned bewerteDiagonal_TopDown(const Spielbrett&sp,FeldStatus fs,
			unsigned limit) const = 0;
public:
	BewertungsFunktion();
	BewertungsFunktion(const BewertungsFunktion &other) = default;
	BewertungsFunktion(BewertungsFunktion &&other) = default;
	virtual ~BewertungsFunktion() = default;
	virtual long long bewerte(const Spielbrett& sb, FeldStatus fs, unsigned limit) const;
	virtual BewertungsFunktion &operator=(const BewertungsFunktion &other) = default;
	virtual BewertungsFunktion &operator=(BewertungsFunktion &&other) = default;

};

} //namespace connect_four
#endif /* BEWERTUNGSFUNKTION_H_ */
