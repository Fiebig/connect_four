/*
 * MinMaxAI.cpp
 *
 *  Created on: 22.12.2014
 *      Author: Alex
 */

#include "MinMaxAI.h"

#include <limits>
#include <iostream>
#include <algorithm>
#include <custom_utils/ThreadPool.h>

#include "BewertungsFunktion.h"
#include "Spielbrett.h"

namespace connect_four{


unsigned MinMaxAI::threadCount = std::max(1u, std::thread::hardware_concurrency());

MinMaxAI::MinMaxAI(FeldStatus spMinSpielsteine_, FeldStatus spMaxSpielsteine_,
		std::unique_ptr<BewertungsFunktion> bf_, unsigned tiefe_, double dismiss_wsk) :
		spMinSpielsteine(spMinSpielsteine_), spMaxSpielsteine(
				spMaxSpielsteine_), bf(std::move(bf_)), globalAlpha(
				std::numeric_limits<long long>::min()), maxTiefe(tiefe_), dismiss_wsk_(dismiss_wsk){
}

/**
 * Calculates next move based on sb and limit.
 * @param sb - current game board
 * @param limit - amount of tokens in sequence required for victory
 * @return column for next move
 */
int MinMaxAI::computeNextMove(Spielbrett& sb, unsigned limit) {
	int nextCol = pvs(-1, sb, limit, spMaxSpielsteine, 0,
			-std::numeric_limits<long long>::max(),
			std::numeric_limits<long long>::max()).second;
	globalAlpha.store(std::numeric_limits<long long>::min());
	return nextCol;
}

/**
 * Executes principal-variation-splitting algorithm on multiple threads in parallel.
 * A ThreadPool is used for the parallel computing.
 * @param currentMove - move applied at current search depth
 * @param sb - current game board
 * @param limit - amount of tokens in sequence required for victory
 * @param currentSpielsteine - the tokens of the current player
 * @param tiefe__ - current search depth level
 * @param alpha - current alpha value
 * @param beta - current beta value
 * @return - std::pair with the first value representing the evaluation score
 * and the second value representing the move belonging to that score
 */
std::pair<long long, int> MinMaxAI::pvs(int currentMove, Spielbrett& sb,
		unsigned limit, FeldStatus currentSpielsteine, unsigned tiefe__,
		long long alpha, long long beta) {
	if (tiefe__ == maxTiefe || sb.isBrettFull()
			|| sb.isWin(currentSpielsteine, limit)) {
		return std::pair<long long, int>(
				bf->bewerte(sb, currentSpielsteine, limit)
						* (maxTiefe - tiefe__ + 1), currentMove);
	}
	// evaluation of first move
	std::vector<unsigned>&& moveOptions = generateMoves(sb);
	sortMoves(sb, currentSpielsteine, limit, moveOptions);
	unsigned firstMove = moveOptions[0];
	makeMove(sb, currentSpielsteine, firstMove);
	//recursion: traverse down the pvs-path
	long long wert = -pvs(firstMove, sb, limit,
			((currentSpielsteine == spMaxSpielsteine) ?
					spMinSpielsteine : spMaxSpielsteine), tiefe__ + 1, -beta,
			-alpha).first;
	undoMove(sb, firstMove);
	if (wert > alpha) {
		alpha = wert;
		if (alpha >= beta) {
			return std::pair<long long, int>(beta, firstMove);
		}
	}
	// evaluation of other moves in parallel
	globalAlpha.store(alpha);
	std::vector<std::future<long long>> futureResults;
	custom_utils::thread_pool pool(MinMaxAI::threadCount);
	for (unsigned i = 1; i < moveOptions.size(); i++) {
		// ignore search path with probability dismiss_wsk_
		if (((double)rand() / (RAND_MAX)) < dismiss_wsk_)
			continue;
		makeMove(sb, currentSpielsteine, moveOptions[i]);
		//parallel
		futureResults.emplace_back(
				pool.enqueue(
						//note: sb pass by value for deep copy
						[this](Spielbrett sb,unsigned limit,FeldStatus currentSpielsteine,
								int tiefe,long long alpha,long long beta)->long long
						{
							if (globalAlpha.load() > alpha) {
								alpha = globalAlpha.load();
							}
							long long wert = -(this->negamax(sb, limit,
											((currentSpielsteine == spMaxSpielsteine) ?
													spMinSpielsteine : spMaxSpielsteine),
											tiefe + 1, -beta, -alpha));
							if (wert > globalAlpha.load()) {
								globalAlpha.store(wert);
							}
							return wert;
						}, sb, limit, currentSpielsteine, tiefe__, alpha,
						beta));
		//end parallel
		undoMove(sb, moveOptions[i]);
	}
	//initial result from pvs-path
	std::pair<long long, int> res(alpha, firstMove);
	// get other results from future
	for (unsigned i = 0; i < futureResults.size(); i++) {
		futureResults[i].wait();
		long long temp = futureResults[i].get();
		if (temp > alpha) {
			alpha = temp;
			if (alpha >= beta) {
				return std::pair<long long, int>(beta, moveOptions[i + 1]);
			}
			res = std::pair<long long, int>(alpha, moveOptions[i + 1]);
		}
	}
	return res;
	//Destruktor of threadpool joins all threads
}

/**
 * Standard alpha-beta-pruning algorithm in negamax style.
 * @param sb - current game board
 * @param limit - amount of tokens in sequence required for victory
 * @param currentSpielsteine - the tokens of the current player
 * @param tiefe__ - current search depth level
 * @param alpha - current alpha value
 * @param beta - current beta value
 * @return evaluation score
 */
long long MinMaxAI::negamax(Spielbrett& sb, unsigned limit,
		FeldStatus currentSpielsteine, unsigned tiefe__, long long alpha,
		long long beta) const {
	if (tiefe__ == maxTiefe || sb.isBrettFull()
			|| sb.isWin(currentSpielsteine, limit)) {
		return bf->bewerte(sb, currentSpielsteine, limit)
				* (maxTiefe - tiefe__ + 1);
	}
	std::vector<unsigned>&& moveOptions = generateMoves(sb);
	sortMoves(sb, currentSpielsteine, limit, moveOptions);
	for (unsigned i = 0; i < moveOptions.size(); i++) {
		unsigned move = moveOptions[i];
		makeMove(sb, currentSpielsteine, move);
		long long wert = -negamax(sb, limit,
				((currentSpielsteine == spMaxSpielsteine) ?
						spMinSpielsteine : spMaxSpielsteine), tiefe__ + 1,
				-beta, -alpha);
		undoMove(sb, move);
		if (wert > alpha) {
			alpha = wert;
			if (alpha >= beta) {
				return beta;
			}
		}
	}
	return alpha;
}

/**
 * Generates all possible moves for sb.
 * @param sb - current game board
 * @return - vector with move options
 */
std::vector<unsigned> MinMaxAI::generateMoves(const Spielbrett& sb) const {
	std::vector<unsigned> res(0);
	for (unsigned i = 0; i < sb.getWidth(); i++) {
		if (sb.getFreeColumnIndex(i) != -1) {
			res.push_back(i);
		}
	}
	return res;
}
/**
 * Executes the specified move on sb.
 * @param sb - current game board
 * @param fs - player token for the move
 * @param col - game board column for the move
 */
void MinMaxAI::makeMove(Spielbrett& sb, FeldStatus fs, unsigned col) const {
	sb.setFeld(col, sb.getFreeColumnIndex(col), fs);
}

/**
 * Reverts the last move on sb in col.
 * @param sb - current game board
 * @param col - game board column in which move is undone
 */
void MinMaxAI::undoMove(Spielbrett& sb, unsigned col) const {
	int temp = sb.getFreeColumnIndex(col);
	sb.setFeld(col, temp + 1, FeldStatus::LEER);
}

/**
 * Presorts the moveOptions to achieve more pruning.
 * @param sb - current game board
 * @param fs - player token for the move
 * @param limit - amount of tokens in sequence required for victory
 * @param moveOptions - moveOptions to be presorted
 */
void MinMaxAI::sortMoves(Spielbrett& sb, FeldStatus fs, unsigned limit,
		std::vector<unsigned>& moveOptions) const {
	std::stable_sort(moveOptions.begin(), moveOptions.end(),
			[&](unsigned a, unsigned b) -> bool
			{
				makeMove(sb,fs,a);
				long long av = bf->bewerte(sb,fs,limit);
				undoMove(sb,a);
				makeMove(sb,fs,b);
				long long bv = bf->bewerte(sb,fs,limit);
				undoMove(sb,b);
				return av > bv;
			});
}

} //namespace connect_four
