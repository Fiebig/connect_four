/*
 * BewertungsFunktion.cpp
 *
 *  Created on: 25.12.2014
 *      Author: Alex
 */
#include "StandardBewertungsFunktion.h"

#include <limits>
#include <cmath>

#include "Spielbrett.h"

namespace connect_four{


static const unsigned power = 10;

StandardBewertungsFunktion::StandardBewertungsFunktion() {
}

unsigned StandardBewertungsFunktion::bewerteHorizontal(const Spielbrett& sb,
		FeldStatus fs, unsigned limit) const {
	unsigned bew = 0;
	unsigned fsCount;
	for (int i = 0; i < sb.height; i++) {
		fsCount = 0;
		for (int j = 0; j < sb.width; j++) {
			if (sb.felder[i][j] != fs) {
				//ende fs segment
				if (fsCount != 0) {
					bew += std::pow(fsCount, power)
							* testHorizontal(sb, i, j, fs, fsCount, limit);
				}
				fsCount = 0;
			} else if (++fsCount == limit) {
				return std::numeric_limits<unsigned>::max();
			}
		}
		if (fsCount != 0) {
			bew += std::pow(fsCount, power)
					* testHorizontal(sb, i, sb.width, fs, fsCount, limit);
		}
	}
	return bew;
}

unsigned StandardBewertungsFunktion::testHorizontal(const Spielbrett& sb, int i,
		int j, FeldStatus fs, unsigned fsCount, unsigned limit) const {
	unsigned res = 2;
	int start = j;
	int stop = j - 1 + (limit - fsCount);
	for (int k = start; k <= stop; k++) {
		if (k >= sb.width
				|| (sb.felder[i][k] != fs && sb.felder[i][k] != FeldStatus::LEER)) {
			res--;
			break;
		}
	}
	start = j - fsCount - 1;
	stop = j - limit;
	for (int k = start; k >= stop; k--) {
		if (k < 0
				|| (sb.felder[i][k] != fs && sb.felder[i][k] != FeldStatus::LEER)) {
			return --res;
		}
	}
	return res;
}

unsigned StandardBewertungsFunktion::bewerteVertikal(const Spielbrett& sb,
		FeldStatus fs, unsigned limit) const {
	unsigned bew = 0;
	unsigned fsCount;
	for (int i = 0; i < sb.width; i++) {
		fsCount = 0;
		for (int j = 0; j < sb.height; j++) {
			if (sb.felder[j][i] != fs) {
				//ende fs segment
				if (fsCount != 0) {
					bew += std::pow(fsCount, power)
							* testVertikal(sb, i, j, fs, fsCount, limit);
				}
				fsCount = 0;
			} else if (++fsCount == limit) {
				return std::numeric_limits<unsigned>::max();
			}
		}
		if (fsCount != 0) {
			bew += std::pow(fsCount, power)
					* testVertikal(sb, i, sb.height, fs, fsCount, limit);
		}
	}
	return bew;
}

unsigned StandardBewertungsFunktion::testVertikal(const Spielbrett& sb, int i,
		int j, FeldStatus fs, unsigned fsCount, unsigned limit) const {
	unsigned res = 2;
	int start = j;
	int stop = j - 1 + (limit - fsCount);
	for (int k = start; k <= stop; k++) {
		if (k >= sb.height
				|| (sb.felder[k][i] != fs && sb.felder[k][i] != FeldStatus::LEER)) {
			res--;
			break;
		}
	}
	start = j - fsCount - 1;
	stop = j - limit;
	for (int k = start; k >= stop; k--) {
		if (k < 0
				|| (sb.felder[k][i] != fs && sb.felder[k][i] != FeldStatus::LEER)) {
			return --res;
		}
	}
	return res;
}

unsigned StandardBewertungsFunktion::bewerteDiagonal_BottumUp(
		const Spielbrett& sb, FeldStatus fs, unsigned limit) const {
	unsigned bew = 0;
	unsigned fsCount;
//Check von unten rechts nach unten links
	for (int i = sb.width - 1; i >= 0; i--) {
		fsCount = 0;
		for (int j = sb.height - 1, temp = i; temp < sb.width && j >= 0;
				j--, temp++) {
			if (sb.felder[j][temp] != fs) {
				//ende fs segment
				if (fsCount != 0) {
					bew += std::pow(fsCount, power)
							* testDiagonal_BottumUp(sb, j, temp, fs, fsCount,
									limit);
				}
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return std::numeric_limits<unsigned>::max();
				}
				//will break
				if (j == 0 || temp == sb.width - 1) {
					bew += std::pow(fsCount, power)
							* testDiagonal_BottumUp(sb, j - 1, temp + 1, fs,
									fsCount, limit);
				}
			}
		}
	}
//Check von unten links nach oben links
	for (int i = sb.height - 2; i >= 0; i--) {
		fsCount = 0;
		for (int j = 0, temp = i; j < sb.width && temp >= 0; j++, temp--) {
			if (sb.felder[temp][j] != fs) {
				//ende fs segment
				if (fsCount != 0) {
					bew += std::pow(fsCount, power)
							* testDiagonal_BottumUp(sb, j, temp, fs, fsCount,
									limit);
				}
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return std::numeric_limits<unsigned>::max();
				}
				//will break
				if (temp == 0 || j == sb.width - 1) {
					bew += std::pow(fsCount, power)
							* testDiagonal_BottumUp(sb, j + 1, temp - 1, fs,
									fsCount, limit);
				}
			}
		}
	}
	return bew;
}

unsigned StandardBewertungsFunktion::testDiagonal_BottumUp(const Spielbrett& sb,
		int j, int temp, FeldStatus fs, unsigned fsCount,
		unsigned limit) const {
	unsigned res = 2;
	int start = j;
	int start2 = temp;
	int stop = j + 1 - (limit - fsCount);
	for (int k = start, l = start2; k >= stop; k--, l++) {
		if (k < 0 || l >= sb.width
				|| (sb.felder[k][l] != fs && sb.felder[k][l] != FeldStatus::LEER)) {
			res--;
			break;
		}
	}
	start = j + fsCount + 1;
	start2 = temp - fsCount - 1;
	stop = j + limit;
	for (int k = start, l = start2; k <= stop; k++, l--) {
		if (k >= sb.height || l < 0
				|| (sb.felder[k][l] != fs && sb.felder[k][l] != FeldStatus::LEER)) {
			return --res;
		}
	}
	return res;
}

unsigned StandardBewertungsFunktion::bewerteDiagonal_TopDown(
		const Spielbrett& sb, FeldStatus fs, unsigned limit) const {
	unsigned bew = 0;
	unsigned fsCount;
//Check von oben rechts nach oben links
	for (int i = sb.width - 1; i >= 0; i--) {
		fsCount = 0;
		for (int j = 0, temp = i; j < sb.height && temp < sb.width;
				j++, temp++) {
			if (sb.felder[j][temp] != fs) {
				//ende fs segment
				if (fsCount != 0) {
					bew += std::pow(fsCount, power)
							* testDiagonal_TopDown(sb, j, temp, fs, fsCount,
									limit);
				}
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return std::numeric_limits<unsigned>::max();
				}
				//will break
				if (j == sb.height - 1 || temp == sb.width - 1) {
					bew += std::pow(fsCount, power)
							* testDiagonal_TopDown(sb, j + 1, temp + 1, fs,
									fsCount, limit);
				}
			}
		}
	}
//Check von oben links nach unten links
	for (int i = 1; i < sb.height; i++) {
		fsCount = 0;
		for (int j = 0, temp = i; temp < sb.height && j < sb.width;
				j++, temp++) {
			if (sb.felder[temp][j] != fs) {
				//ende fs segment
				if (fsCount != 0) {
					bew += std::pow(fsCount, power)
							* testDiagonal_TopDown(sb, j, temp, fs, fsCount,
									limit);
				}
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return std::numeric_limits<unsigned>::max();
				}
				//will break
				if (temp == sb.height - 1 || j == sb.width - 1) {
					bew += std::pow(fsCount, power)
							* testDiagonal_TopDown(sb, j + 1, temp + 1, fs,
									fsCount, limit);
				}
			}
		}
	}
	return bew;
}

unsigned StandardBewertungsFunktion::testDiagonal_TopDown(const Spielbrett& sb,
		int j, int temp, FeldStatus fs, unsigned fsCount,
		unsigned limit) const {
	unsigned res = 2;
	int start = j;
	int start2 = temp;
	int stop = j - 1 + (limit - fsCount);
	for (int k = start, l = start2; k <= stop; k++, l++) {
		if (k >= sb.height || l >= sb.width
				|| (sb.felder[k][l] != fs && sb.felder[k][l] != FeldStatus::LEER)) {
			res--;
			break;
		}
	}
	start = j - fsCount - 1;
	start2 = temp - fsCount - 1;
	stop = j - limit;
	for (int k = start, l = start2; k >= stop; k--, l--) {
		if (k < 0 || l < 0
				|| (sb.felder[k][l] != fs && sb.felder[k][l] != FeldStatus::LEER)) {
			return --res;
		}
	}
	return res;
}

} //namespace connect_four
