/*
 * ProjectExceptions.h
 *
 *  Created on: 22.12.2014
 *      Author: Alex
 */

#ifndef PROJECTEXCEPTIONS_H_
#define PROJECTEXCEPTIONS_H_

#include <stdexcept>
#include <string>

namespace connect_four{


class InvalidParameterException: public std::exception{
	const std::string msg;
public:
	InvalidParameterException(const std::string &message_);
	const char* what() const noexcept override;
};


} //namespace connect_four
#endif /* PROJECTEXCEPTIONS_H_ */
