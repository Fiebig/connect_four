/*
 * Spiel.cpp
 *
 *  Created on: 21.12.2014
 *      Author: Alex
 */
#include "Spiel.h"

#include <limits>

#include "ProjectExceptions.h"

namespace connect_four{


/** Creates Default Connect 4-Game.
 * @param sp1 - Spieler 1
 * @param sp2 - Spieler 2
 */
Spiel::Spiel(Spieler* sp1, Spieler* sp2) :
    Spiel(sp1, sp2, 7, 6, 4) {
}
/**
 * Creates Custom Connect 4-Game.
 * @param sp1 - Spieler 1
 * @param sp2 - Spieler 2
 * @param sbWidth - Breite des Spielbretts
 * @param sbHeight - Hoehe des Spielbretts
 * @param limit_ - Siegbedingung
 */
Spiel::Spiel(Spieler* sp1, Spieler* sp2, unsigned sbWidth, unsigned sbHeight,
             unsigned limit_) :
    sb(std::unique_ptr<Spielbrett>(new Spielbrett(sbWidth, sbHeight))),
    gameFinished(
        false), limit(limit_) {
    if (*sp1 == *sp2)
        throw InvalidParameterException("Spieler sind identisch!");
    //create cycle
    sp1->next = sp2;
    sp2->next = sp1;
    spielerAmZug = sp1;
}

/*Move Constructor*/
Spiel::Spiel(Spiel&& other) noexcept:
    sb(std::move(other.sb)), spielerAmZug(other.spielerAmZug),
    gameFinished(other.gameFinished), limit(other.limit) {
    other.spielerAmZug = nullptr;
    other.gameFinished = false;
    other.limit = 0;
}

Spiel::~Spiel() noexcept {
    //break cycle;
    Spieler* temp = spielerAmZug->next;
    spielerAmZug->next = nullptr;
    //delete
    delete temp;
    spielerAmZug = nullptr;
}

/*Move assignment operator*/
Spiel& Spiel::operator=(Spiel&& other) noexcept {
    if (this != &other) {
        //break cycle;
        Spieler* temp = spielerAmZug->next;
        spielerAmZug->next = nullptr;
        //delete
        delete temp;
        sb = std::move(other.sb);
        spielerAmZug = other.spielerAmZug;
        gameFinished = other.gameFinished;
        limit = other.limit;
        other.spielerAmZug = nullptr;
        other.gameFinished = false;
        other.limit = 0;
    }
    return *this;
}

const Spielbrett& Spiel::getSb() const {
    return *sb;
}

const Spieler& Spiel:: getCurrentPlayer() const {
    return *spielerAmZug;
}

unsigned Spiel::getLimit() const {
    return limit;
}

bool Spiel::isGameFinished() const {
    return gameFinished;
}

/**Starts the Game and runs it until its finished.*/
void Spiel:: nextPlayerMove(unsigned col) {
    if (!gameFinished) {
        spielerAmZug->makeMove(*sb, limit, col);
        if (!( gameFinished = isRemis() || hasCurrentPlayerWon()))
            spielerAmZug = spielerAmZug->next;
    }
}

/**
 * Brings the Game back into start state so it can be restarted.
 * Player to begin is the player who lost last time.
 */
void Spiel::toStartState() {
    sb->clearBrett();
    spielerAmZug = spielerAmZug->next;
    gameFinished = false;
}

bool Spiel::isRemis() const {
    return sb->isBrettFull() && !hasCurrentPlayerWon();
}

bool Spiel::hasCurrentPlayerWon() const {
    return sb->isWin(spielerAmZug->getSpielsteine(), limit);
}

} //namespace connect_four
