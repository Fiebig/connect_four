/*
 * MinMaxAI.h
 *
 *  Created on: 22.12.2014
 *      Author: Alex
 */

#ifndef MINMAXAI_H_
#define MINMAXAI_H_

#include <vector>
#include <atomic>
#include <memory>

namespace connect_four{


class BewertungsFunktion;
class Spielbrett;
enum class FeldStatus;

class MinMaxAI {
private:
	FeldStatus spMinSpielsteine,spMaxSpielsteine;
	std::unique_ptr<BewertungsFunktion> bf;
	std::atomic<long long> globalAlpha;
	unsigned maxTiefe;
	double dismiss_wsk_;
public:
	static unsigned threadCount;
	MinMaxAI(FeldStatus spMinSpielsteine_, FeldStatus spMaxSpielsteine_,
			std::unique_ptr<BewertungsFunktion> bf_, unsigned tiefe_, double dismiss_wsk);
	MinMaxAI(const MinMaxAI &other) = delete;
	MinMaxAI(MinMaxAI &&other) noexcept = default;
	~MinMaxAI() = default;
	MinMaxAI& operator=(const MinMaxAI &other) = delete;
	MinMaxAI& operator=(MinMaxAI &&other) noexcept = default;
	int computeNextMove(Spielbrett& sb, unsigned limit);
private:
	std::pair<long long, int> pvs(int currentMove, Spielbrett& sb,
				unsigned limit, FeldStatus currentSpielsteine, unsigned tiefe__,
				long long alpha, long long beta);
	long long negamax(Spielbrett& sb, unsigned limit,
				FeldStatus currentSpielsteine, unsigned tiefe__, long long alpha,
				long long beta) const;
	std::vector<unsigned> generateMoves(const Spielbrett& sb) const;
	void makeMove(Spielbrett& sb, FeldStatus fs, unsigned col) const;
	void undoMove(Spielbrett& sb, unsigned col) const;
	void sortMoves(Spielbrett&sb, FeldStatus fs, unsigned limit,
				std::vector<unsigned>& moves) const;
};


} //namespace connect_four
#endif /* MINMAXAI_H_ */

