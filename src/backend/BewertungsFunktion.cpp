/*
 * BewertungsFunktion.cpp
 *
 *  Created on: 25.12.2014
 *      Author: Alex
 */

#include "BewertungsFunktion.h"

namespace connect_four{


BewertungsFunktion::BewertungsFunktion() {
}

long long BewertungsFunktion::bewerte(const Spielbrett& sb, FeldStatus fs,
		unsigned limit) const {
	return (long long) bewerteHorizontal(sb, fs, limit)
			+ (long long) bewerteVertikal(sb, fs, limit)
			+ (long long) bewerteDiagonal_BottumUp(sb, fs, limit)
			+ (long long) bewerteDiagonal_TopDown(sb, fs, limit);
}

} //namespace connect_four
