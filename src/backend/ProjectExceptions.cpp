/*
 * ProjectExceptions.cpp
 *
 *  Created on: 22.12.2014
 *      Author: Alex
 */

#include "ProjectExceptions.h"

namespace connect_four{


InvalidParameterException::InvalidParameterException
(const std::string &message_):msg(message_) {
}

const char* InvalidParameterException::what() const noexcept{
	return msg.c_str();
}


} //namespace connect_four
