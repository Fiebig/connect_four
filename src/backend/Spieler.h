/*
 * Spieler.h
 *
 *  Created on: 04.12.2014
 *      Author: Alex
 */

#ifndef SPIELER_H_
#define SPIELER_H_

#include <memory>
#include <string>

namespace connect_four{


enum class FeldStatus;
class Spielbrett;
/**
 **********************************************
 * @class Spieler
 * Abstract class representing a single player.
 **********************************************
 */
class Spieler {
    protected:
        std::string name;
        FeldStatus spielsteine;
    public:
        Spieler* next;
        Spieler(const std::string& name_, FeldStatus spielsteine_);
        Spieler(Spieler&& other);
        Spieler(const Spieler& other) = delete;
        virtual ~Spieler() noexcept;
        const std::string& getName() const;
        FeldStatus getSpielsteine() const;
        /**
         * Player makes next move.
         * @param sb - board on which move should be made
         * @param limit - amount of tokens in sequence required for victory
         */
        virtual void makeMove(Spielbrett& sb, unsigned limit, unsigned col) const = 0;
        Spieler& operator=(const Spieler& other) = delete;
        Spieler& operator=(Spieler&& other);
        bool operator==(const Spieler& other) const;
        bool operator!=(const Spieler& other) const;
        friend std::ostream& operator<<(std::ostream& os, const Spieler& spieler);
};

} //namespace connect_four
#endif /* SPIELER_H_ */
