/*
 * BewertungsFunktion.h
 *
 *  Created on: 23.12.2014
 *      Author: Alex
 */

#ifndef STANDARDBEWERTUNGSFUNKTION_H_
#define STANDARDBEWERTUNGSFUNKTION_H_

#include "BewertungsFunktion.h"

namespace connect_four{


class StandardBewertungsFunktion: public BewertungsFunktion {
	unsigned testHorizontal(const Spielbrett& sb,int i, int j, FeldStatus fs, unsigned fsCount,
			unsigned limit) const;
	unsigned testVertikal(const Spielbrett& sb,int i, int j, FeldStatus fs, unsigned fsCount,
			unsigned limit) const;
	unsigned testDiagonal_BottumUp(const Spielbrett& sb,int j, int temp, FeldStatus fs, unsigned fsCount,
			unsigned limit) const;
	unsigned testDiagonal_TopDown(const Spielbrett& sb,int j, int temp, FeldStatus fs, unsigned fsCount,
			unsigned limit) const;
public:
	StandardBewertungsFunktion();

	unsigned bewerteHorizontal(const Spielbrett& sb,FeldStatus fs, unsigned limit) const override;

	unsigned bewerteVertikal(const Spielbrett& sb,FeldStatus fs, unsigned limit) const override;

	unsigned bewerteDiagonal_BottumUp(const Spielbrett& sb,FeldStatus fs, unsigned limit) const
			override;

	unsigned bewerteDiagonal_TopDown(const Spielbrett& sb,FeldStatus fs, unsigned limit) const
			override;
};

} //namespace connect_four
#endif /* STANDARDBEWERTUNGSFUNKTION_H_ */
