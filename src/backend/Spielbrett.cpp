/*
 * Spielbrett.cpp
 *
 *  Created on: 02.11.2014
 *      Author: Alex
 */
#include "Spielbrett.h"

#include <string>

#include "ProjectExceptions.h"


namespace connect_four{


/**
 *Benutzerdefiniertes Spielbrett
 */
Spielbrett::Spielbrett(unsigned width_, unsigned height_) :
		height(height_), width(width_), felder(new FeldStatus*[height_]), columnCount(
				new int[width_]) {
	for (int i = 0; i < height; i++) {
		felder[i] = new FeldStatus[width];
		for (int j = 0; j < width; j++) {
			felder[i][j] = FeldStatus::LEER;
		}
	}
	for (int i = 0; i < width; i++) {
		columnCount[i] = 0;
	}
}
/**
 * Copy Constructor
 */
Spielbrett::Spielbrett(const Spielbrett &other) :
		height(other.height), width(other.width), felder(
				new FeldStatus*[other.height]), columnCount(
				new int[other.width]) {
	for (int i = 0; i < height; i++) {
		felder[i] = new FeldStatus[width];
		for (int j = 0; j < width; j++) {
			felder[i][j] = other.felder[i][j];
		}
	}
	for (int i = 0; i < width; i++) {
		columnCount[i] = other.columnCount[i];
	}
}

/**
 * Move constructor
 */
Spielbrett::Spielbrett(Spielbrett&&other) noexcept :
height(other.height),width(other.width), felder(other.felder),columnCount(other.columnCount) {
	other.width = 0;
	other.height = 0;
	other.felder = nullptr;
	other.columnCount = nullptr;
}

Spielbrett::~Spielbrett() noexcept {
	for (int i = 0; i < height; i++) {
		delete[] felder[i];
	}
	delete[] felder;
	delete[] columnCount;
}

FeldStatus Spielbrett::getFeld(int x, int y) const {
	if (x >= width || y >= height || x < 0 || y < 0) {
		throw InvalidParameterException(
				"Feld " + std::to_string(x) + "," + std::to_string(y)
						+ " existiert nicht!");
	}
	return felder[y][x];
}

void Spielbrett::setFeld(int x, int y, FeldStatus fs) {
	if (x >= width || y >= height || x < 0 || y < 0) {
		throw InvalidParameterException(
				"Feld " + std::to_string(x) + "," + std::to_string(y)
						+ " existiert nicht!");
	}
	if (fs != FeldStatus::LEER && felder[y][x] == FeldStatus::LEER) {
		columnCount[x]++;
	} else if (fs == FeldStatus::LEER && felder[y][x] != FeldStatus::LEER) {
		columnCount[x]--;
	}
	felder[y][x] = fs;
}

int Spielbrett::getFreeColumnIndex(int column) const {
	if (column >= width || column < 0) {
		throw InvalidParameterException(
				"Spalte " + std::to_string(column) + " existiert nicht!");
	}
	return height - 1 - columnCount[column];
}

unsigned Spielbrett::getHeight() const {
	return height;
}

unsigned Spielbrett::getWidth() const {
	return width;
}

void Spielbrett::clearBrett() {
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			felder[i][j] = FeldStatus::LEER;
		}
	}
	for (int i = 0; i < width; i++) {
		columnCount[i] = 0;
	}
}

bool Spielbrett::isBrettFull() const {
	for (int i = 0; i < width; i++) {
		if (columnCount[i] != height) {
			return false;
		}
	}
	return true;
}

bool Spielbrett::isWin(FeldStatus fs, unsigned limit) const {
	return isVertikalWin(fs, limit) || isHorizontalWin(fs, limit)
			|| isDiagonalWin_BottumUp(fs, limit)
			|| isDiagonalWin_TopDown(fs, limit);
}

bool Spielbrett::isHorizontalWin(FeldStatus fs, unsigned limit) const {
	unsigned fsCount;
	for (int i = 0; i < height; i++) {
		fsCount = 0;
		for (int j = 0; j < width; j++) {
			if (felder[i][j] != fs) {
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return true;
				}
			}
		}
	}
	return false;
}

bool Spielbrett::isVertikalWin(FeldStatus fs, unsigned limit) const {
	unsigned fsCount;
	for (int i = 0; i < width; i++) {
		fsCount = 0;
		for (int j = 0; j < height; j++) {
			if (felder[j][i] != fs) {
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return true;
				}
			}
		}
	}
	return false;
}

bool Spielbrett::isDiagonalWin_BottumUp(FeldStatus fs, unsigned limit) const {
	unsigned fsCount;
	//Check von unten rechts nach unten links
	for (int i = width - 1; i >= 0; i--) {
		fsCount = 0;
		for (int j = height - 1, temp = i; temp < width && j >= 0;
				j--, temp++) {
			if (felder[j][temp] != fs) {
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return true;
				}
			}
		}
	}
	//Check von unten links nach oben links
	for (int i = height - 2; i >= 0; i--) {
		fsCount = 0;
		for (int j = 0, temp = i; j < width && temp >= 0; j++, temp--) {
			if (felder[temp][j] != fs) {
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return true;
				}
			}
		}
	}
	return false;
}

bool Spielbrett::isDiagonalWin_TopDown(FeldStatus fs, unsigned limit) const {
	unsigned fsCount;
	//Check von oben rechts nach oben links
	for (int i = width - 1; i >= 0; i--) {
		fsCount = 0;
		for (int j = 0, temp = i; j < height && temp < width; j++, temp++) {
			if (felder[j][temp] != fs) {
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return true;
				}
			}
		}
	}
	//Check von oben links nach unten links
	for (int i = 1; i < height; i++) {
		fsCount = 0;
		for (int j = 0, temp = i; temp < height && j < width; j++, temp++) {
			if (felder[temp][j] != fs) {
				fsCount = 0;
			} else {
				if (++fsCount == limit) {
					return true;
				}
			}
		}
	}
	return false;
}

/**
 * Copy + Move assignment operator
 * (Unifying assignment operator)
 */
Spielbrett& Spielbrett::operator=(Spielbrett other) noexcept {
	swap(*this, other);
	return *this;
}

bool Spielbrett::operator ==(const Spielbrett& other) const {
	if (this != &other) {
		if (height != other.height || width != other.width) {
			return false;
		}
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (felder[i][j] != other.felder[i][j]) {
					return false;
				}
			}
		}
	}
	return true;
}

bool Spielbrett::operator !=(const Spielbrett& other) const {
	if (this != &other) {
		if (height != other.height || width != other.width) {
			return true;
		}
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (felder[i][j] != other.felder[i][j]) {
					return true;
				}
			}
		}
	}
	return false;
}

/**
 * Allows direct readonly access
 */
const FeldStatus* Spielbrett::operator[](int i) const {
	return felder[i];
}

void swap(Spielbrett& first, Spielbrett& second) noexcept {
	std::swap(first.height, second.height);
	std::swap(first.width, second.width);
	std::swap(first.felder, second.felder);
	std::swap(first.columnCount, second.columnCount);
}

std::ostream& operator<<(std::ostream&os, const Spielbrett& spielbrett) {
	os << std::endl;
	for (int i = 0; i < spielbrett.width; i++) {
		os << "====";
	}
	os << std::endl;

	for (int i = 0; i < spielbrett.width; i++) {
		os << ' ' << i << "  ";
	}
	os << std::endl;
	for (int i = 0; i < spielbrett.width; i++) {
		os << "====";
	}
	os << std::endl;
	for (int i = 0; i < spielbrett.height; i++) {
		for (int j = 0; j < spielbrett.width; j++) {
			os << '[' << spielbrett[i][j] << ']' << ' ';
		}
		os << std::endl;
	}
	for (int i = 0; i < spielbrett.width; i++) {
		os << "====";
	}
	os << std::endl;
	return os;
}

std::ostream& operator<<(std::ostream &os, FeldStatus feldstatus) {
	switch (feldstatus) {
	case FeldStatus::SPIELER1:
		os << 'O';
		break;
	case FeldStatus::SPIELER2:
		os << 'X';
		break;
	default:
		os << ' ';
	}
	return os;
}

} //namespace connect_four
