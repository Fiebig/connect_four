/*
 * Spielbrett.h
 *
 *  Created on: 02.11.2014
 *      Author: Alex
 */

#ifndef SPIELBRETT_H_
#define SPIELBRETT_H_

#include <iostream>


namespace connect_four{


enum class FeldStatus {
	LEER = 0, SPIELER1 = 1, SPIELER2 = 2
};
std::ostream&operator<<(std::ostream&os, FeldStatus feldstatus);

class Spielbrett {
	friend class StandardBewertungsFunktion;
	int height, width;
	FeldStatus** felder;
	int* columnCount;
public:
	Spielbrett(unsigned width = 7, unsigned height = 6);
	Spielbrett(const Spielbrett & other);
	Spielbrett(Spielbrett &&other) noexcept;
	~Spielbrett() noexcept;
	FeldStatus getFeld(int x, int y) const;
	void setFeld(int x, int y, FeldStatus fs);
	int getFreeColumnIndex(int column) const;
	unsigned getHeight() const;
	unsigned getWidth() const;
	void clearBrett();
	bool isBrettFull() const;
	bool isWin(FeldStatus fs, unsigned limit) const;
	bool isHorizontalWin(FeldStatus fs, unsigned limit) const;
	bool isVertikalWin(FeldStatus fs, unsigned limit) const;
	bool isDiagonalWin_BottumUp(FeldStatus fs, unsigned limit) const;
	bool isDiagonalWin_TopDown(FeldStatus fs, unsigned limit) const;
	Spielbrett& operator=(Spielbrett other) noexcept;
	const FeldStatus* operator[](int i) const;
	bool operator==(const Spielbrett& other) const;
	bool operator!=(const Spielbrett& other) const;
	friend void swap(Spielbrett& first, Spielbrett& second) noexcept;
	friend std::ostream& operator<<(std::ostream& os,
			const Spielbrett& spielbrett);
};

} //namespace connect_four
#endif /* SPIELBRETT_H_ */
