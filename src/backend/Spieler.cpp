/*
 * Spieler.cpp
 *
 *  Created on: 11.12.2014
 *      Author: Alex
 */

#include "Spieler.h"

#include "ProjectExceptions.h"
#include "Spielbrett.h"


namespace connect_four{


Spieler::Spieler(const std::string& name_, FeldStatus spielsteine_) :
    name(name_), spielsteine(spielsteine_), next(nullptr) {
    if (spielsteine == FeldStatus::LEER) {
        throw InvalidParameterException(
            "Spieler hat ungueltigen Spielsteintyp!");
    }
}

/*Move constructor*/
Spieler::Spieler(Spieler&& other) :
    name(other.name), spielsteine(other.spielsteine), next(other.next) {
    other.name = "";
    other.spielsteine = FeldStatus::LEER;
    other.next = nullptr;
}

Spieler::~Spieler() noexcept {
    delete next;
}

const std::string& Spieler::getName() const {
    return name;
}

FeldStatus Spieler::getSpielsteine() const {
    return spielsteine;
}

Spieler& Spieler::operator=(Spieler&& other) {
    if (this != &other) {
        delete next;
        name = other.name;
        spielsteine = other.spielsteine;
        next = other.next;
        other.name = "";
        other.spielsteine = FeldStatus::LEER;
        other.next = nullptr;
    }
    return *this;
}

bool Spieler::operator==(const Spieler& other) const {
    return name == other.name;
}

bool Spieler::operator!=(const Spieler& other) const {
    return name != other.name;
}

std::ostream& operator<<(std::ostream& os, const Spieler& spieler) {
    os << spieler.name;
    return os;
}

} //namespace connect_four
