#ifndef NEWGAMEWINDOW_H
#define NEWGAMEWINDOW_H

#include <QWidget>

#include "backend/AiSpieler.h"


namespace connect_four{

class Spiel;

enum class PlayerType {
    AI = 0, MENSCH = 1
};



class NewGameWindow : public QWidget {
        Q_OBJECT
        void initWindow();
    public:
        NewGameWindow(QWidget* parent = 0, Qt::WindowFlags f = 0);
    private slots:
        void createGame();
    signals:
        void gameCreated(Spiel* game);
};

} //namespace connect_four


Q_DECLARE_METATYPE(connect_four::PlayerType)
Q_DECLARE_METATYPE(connect_four::Difficulty)


#endif // NEWGAMEWINDOW_H

