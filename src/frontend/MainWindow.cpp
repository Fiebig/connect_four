#include "MainWindow.h"

#include <QMenu>
#include <QStatusBar>
#include <QMenuBar>
#include <QApplication>
#include <QGraphicsView>
#include <QResizeEvent>

#include "BoardScreen.h"
#include "GameAdapter.h"
#include "NewGameWindow.h"
#include "GameFinishedWindow.h"

namespace connect_four{


MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent), gameAdapter(nullptr) {
    this->setWindowTitle("Qt Vier Gewinnt");
    BoardScreen* scene = new BoardScreen();
    QObject::connect( scene , &BoardScreen::itemMoveRequest, this,
                      &MainWindow::executeMove);
	QGraphicsView* view = new QGraphicsView();
	view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setAlignment(Qt::AlignCenter);
	view->setScene(scene);
    this->setCentralWidget(view);
    createMenus();
    createStatusBar();
    qRegisterMetaType<Spielbrett>("Spielbrett");
}

MainWindow::~MainWindow() {
    delete gameAdapter;
}

void MainWindow::createMenus() {
    this-> menuBar();
    QMenu* game = new QMenu("Spiel");
    game->addAction("New Game", this, SLOT(openNewGameWindow()), QKeySequence::New);
    game->addAction("Quit", qApp, SLOT(quit()), QKeySequence::Quit);
    menuBar()->addMenu(game);
}

void MainWindow::createStatusBar() {
    this->statusBar();
    statusBar()->showMessage(tr("Noch kein Spiel erstellt!"));
}


void MainWindow::executeMove(unsigned col) {
    gameAdapter->humanMove(col);
}

void MainWindow::resizeEvent(QResizeEvent *event) {
	event->accept();
	if (event->oldSize().isValid()) {
		QGraphicsView* view = qobject_cast<QGraphicsView*>(this->centralWidget());
		BoardScreen* scene = qobject_cast<BoardScreen*>(view->scene());
		view->fitInView(scene->itemsBoundingRect(), Qt::KeepAspectRatio);
		view->centerOn(0, 0);
	}
}

void MainWindow:: openNewGameWindow() {
    NewGameWindow* ngw = new NewGameWindow();
    QObject::connect( ngw , &NewGameWindow::gameCreated, this,
                      &MainWindow::initGame);
    ngw->setAttribute(Qt::WA_DeleteOnClose);
    ngw->setAttribute(Qt::WA_QuitOnClose, false);
    ngw->show();
}

void MainWindow:: openGameFinishedWindow(const QString& msg) {
    GameFinishedWindow* gfw = new GameFinishedWindow(msg);
    QObject::connect(gfw, &GameFinishedWindow::newGameRequest, this,
                     &MainWindow::openNewGameWindow);
    gfw->setAttribute(Qt::WA_DeleteOnClose);
    gfw->setAttribute(Qt::WA_QuitOnClose, false);
    gfw->show();
}

void MainWindow:: initGame(Spiel* game) {
    //Delete old Game if one exists
    delete gameAdapter;
    //Init new Game
    gameAdapter = new GameAdapter(game);
    QGraphicsView* view = qobject_cast<QGraphicsView*>(this->centralWidget());
    BoardScreen* scene = qobject_cast<BoardScreen*>(view->scene());
    scene->initBoard(gameAdapter->getSb());
	view->setSceneRect(scene->itemsBoundingRect());
	view->fitInView(scene->itemsBoundingRect(), Qt::KeepAspectRatio);
	view->centerOn(0, 0);
    //Set Connections which inform GUI-Thread about new GameStates
    QObject::connect(gameAdapter, &GameAdapter::boardChanged, scene,
                     &BoardScreen::refreshBoard, Qt::QueuedConnection);
    statusBar()->showMessage((gameAdapter->getCurrentPlayer().getName() +
                              " ist am Zug!").c_str());
    QObject::connect(gameAdapter, &GameAdapter::boardChanged, this,
                     [this] {statusBar()->showMessage((gameAdapter->getCurrentPlayer().getName() + " ist am Zug!").c_str());},
                     Qt::QueuedConnection);
    QObject::connect(gameAdapter, &GameAdapter::gameFinished, this,
                     &MainWindow::openGameFinishedWindow, Qt::QueuedConnection);
    //Start Thread
    gameAdapter->start();
}

} //namespace connect_four

