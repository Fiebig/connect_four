#ifndef COLUMNSELECTIONITEM_H
#define COLUMNSELECTIONITEM_H

#include<QGraphicsPolygonItem>

namespace connect_four{


class ColumnSelectionItem : public QObject, public QGraphicsPolygonItem {
        Q_OBJECT
        unsigned col;
    public:
        ColumnSelectionItem(unsigned col_, const QPolygonF& polygon,
                            QGraphicsItem* parent = 0);
        void mousePressEvent ( QGraphicsSceneMouseEvent* event ) override;
    signals:
        void moveRequest(unsigned col);
};



} //namespace connect_four
#endif // COLUMNSELECTIONITEM_H

