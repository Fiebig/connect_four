
#include "GameFinishedWindow.h"

#include <QApplication>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QFont>

namespace connect_four{


GameFinishedWindow::GameFinishedWindow(const QString& message, QWidget* parent,
                                       Qt::WindowFlags f)
    : QWidget(parent, f) {
    this->setWindowTitle(tr("Game Finished"));
    QVBoxLayout* vBoxLayout = new QVBoxLayout(this);
    this->setLayout(vBoxLayout);
    QLabel* msgLabel = new QLabel(message);
    QFont font("Arial", 28, QFont::Bold);
    msgLabel->setFont(font);
    vBoxLayout->addWidget(msgLabel, Qt::AlignCenter);
    QHBoxLayout* hBoxLayout = new QHBoxLayout();
    QPushButton* pb1 = new QPushButton("Quit Game");
    QObject::connect(pb1, &QPushButton::clicked, qApp, &QApplication::quit);
    QPushButton* pb2 = new QPushButton("New Game");
    QObject::connect(pb2, &QPushButton::clicked, this,
                     &GameFinishedWindow::newGameRequest);
    QObject::connect(pb2, &QPushButton::clicked, this, &GameFinishedWindow::close); 
    hBoxLayout->addWidget(pb1);
    hBoxLayout->addWidget(pb2);
    vBoxLayout->addLayout(hBoxLayout);
}

} //namespace connect_four
