#include "BoardScreen.h"

#include <QGraphicsView>

#include "ColumnSelectionItem.h"
#include "backend/Spielbrett.h"


namespace connect_four{


BoardScreen::BoardScreen(QObject* parent): QGraphicsScene(parent) {
    this->setBackgroundBrush(Qt::blue);
}


void BoardScreen::refreshBoard(const Spielbrett& sb) {
    for (unsigned c = 0; c < sb.getWidth(); c++) {
        for (unsigned r = 0; r < sb.getHeight(); r++) {
            QGraphicsItem* item = this->itemAt(xFromCol(c), yFromRow(r + 1), QTransform());
            QGraphicsEllipseItem* ell = qgraphicsitem_cast<QGraphicsEllipseItem*>(item);
            QBrush brush = (sb[r][c] == FeldStatus::LEER) ? Qt::white :
                           (sb[r][c] == FeldStatus::SPIELER1) ? Qt::red : Qt::yellow;
            ell->setBrush(brush);
            ell->update(c * ellPos, (r + 1)*ellPos, ellSize, ellSize);
        }
    }
}

void BoardScreen::initBoard(const Spielbrett& sb) {
    clearBoard();
	QGraphicsItem* root_item = new QGraphicsRectItem();
	const QSize& vs = this->views().back()->size();
    ellPos = std::min(vs.width(), vs.height()) / std::max(sb.getHeight(), sb.getWidth());
    ellSize = (ellPos / 3) * 2;
    for (unsigned c = 0; c < sb.getWidth(); c++) {
        // Describe a closed triangle
        QPolygonF triangle;
        triangle.append(QPointF(c * ellPos, 0));
        triangle.append(QPointF(c * ellPos + ellSize, 0));
        triangle.append(QPointF(c * ellPos + ellSize / 2, ellSize));
        triangle.append(QPointF(c * ellPos, 0));
        // Add the triangle polygon to the scene
        ColumnSelectionItem* triangleItem = new ColumnSelectionItem(c, triangle, root_item);
        triangleItem->setBrush(Qt::gray);
        triangleItem->setCacheMode(QGraphicsItem::NoCache);
        QObject::connect( triangleItem , &ColumnSelectionItem::moveRequest, this,
                          &BoardScreen::itemMoveRequest);
        //
        for (unsigned r = 0; r < sb.getHeight(); r++) {
            QGraphicsEllipseItem* ellItem = new QGraphicsEllipseItem(c * ellPos,
                    (r + 1)*ellPos, ellSize, ellSize, root_item);
            QBrush brush = (sb[r][c] == FeldStatus::LEER) ? Qt::white :
                           (sb[r][c] == FeldStatus::SPIELER1) ? Qt::red : Qt::yellow;
            ellItem->setBrush(brush);
            ellItem->setCacheMode(QGraphicsItem::NoCache);
        }
    }
	this->addItem(root_item);
    this->update(0, 0, width(), height());
}


void BoardScreen::clearBoard() {
    qDeleteAll( this->items() );
}

int BoardScreen:: rowFromPoint(int y) const {
    return y / (ellSize + (ellPos - ellSize) / 2);
}

int BoardScreen:: colFromPoint(int x) const {
    return x / (ellSize + (ellPos - ellSize) / 2);
}

int BoardScreen:: yFromRow(int r) const {
    return r * ellPos + 0.5 * ellSize;
}

int BoardScreen:: xFromCol(int c) const {
    return c * ellPos + 0.5 * ellSize;
}


} //namespace connect_four
