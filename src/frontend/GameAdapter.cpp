
#include "GameAdapter.h"

#include <typeinfo>
#include <QMutexLocker>

#include "backend/AiSpieler.h"
#include "MainWindow.h"

namespace connect_four{


GameAdapter::GameAdapter(Spiel* s):
    QThread(nullptr), game(std::unique_ptr<Spiel>(s)), m_interrupt(false) {
}

GameAdapter::~GameAdapter() {
    if (this->isRunning()) {
        m_interrupt.store(true);
        nextAiMove.wakeOne();
        this->wait();
    }
}

void GameAdapter::run() {
    while (true) {
        {
            QMutexLocker lock(&mutexGame);
            if (typeid(game->getCurrentPlayer()) != typeid(AiSpieler)
                    && !m_interrupt.load() )
                nextAiMove.wait(&mutexGame);
            if (m_interrupt.load())
                break;
            //make move
            nextPlayerMove(-1);
        }
        //sleep
        thread()->sleep(1);
    }
}

const Spielbrett& GameAdapter::getSb() {
    QMutexLocker lock(&mutexGame);
    return game->getSb();
}

const Spieler& GameAdapter::getCurrentPlayer()  {
    QMutexLocker lock(&mutexGame);
    return game->getCurrentPlayer();
}

bool GameAdapter::isGameFinished()  {
    QMutexLocker lock(&mutexGame);
    return game->isGameFinished();
}

void GameAdapter::humanMove(unsigned col) {
    if (typeid(game->getCurrentPlayer()) != typeid(AiSpieler) &&
            mutexGame.tryLock()) {
        //make move
        nextPlayerMove(col);
        //wake up thread if necassary
        if (typeid(game->getCurrentPlayer()) == typeid(AiSpieler)
                || game->isGameFinished())
            nextAiMove.wakeOne();
        mutexGame.unlock();
    }
}


void GameAdapter::nextPlayerMove(unsigned col) {
    game->nextPlayerMove(col);
    emit boardChanged(game->getSb());
    //Check if game is finished
    if (game->isGameFinished()) {
        m_interrupt.store(true);
        if (game->isRemis())
            emit gameFinished("Unentschieden!");
        else
            emit gameFinished((game->getCurrentPlayer().getName() +
                               " hat gewonnen!").c_str());
    }
}

} //namespace connect_four
