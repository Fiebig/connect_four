
#include "ColumnSelectionItem.h"

namespace connect_four{


ColumnSelectionItem::ColumnSelectionItem(unsigned col_,
        const QPolygonF& polygon, QGraphicsItem* parent)
    : QGraphicsPolygonItem(polygon, parent), col(col_) {
    this->setAcceptedMouseButtons(Qt::LeftButton);
}

void ColumnSelectionItem::mousePressEvent ( QGraphicsSceneMouseEvent* event ) {
    emit moveRequest(col);
    QGraphicsPolygonItem::mousePressEvent(event);
}

} //namespace connect_four
