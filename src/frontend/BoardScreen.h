#ifndef BOARDSCREEN_H
#define BOARDSCREEN_H

#include <QGraphicsScene>

namespace connect_four{


class Spielbrett;

class BoardScreen : public QGraphicsScene {
        Q_OBJECT
    public:
        BoardScreen(QObject* parent = 0);
        void initBoard(const Spielbrett& sb);
    public slots:
        void refreshBoard(const Spielbrett& sb);
    private:
        void clearBoard();
        int rowFromPoint(int y) const;
        int colFromPoint(int x) const;
        int xFromCol(int c) const;
        int yFromRow(int r) const;
    signals:
        void itemMoveRequest(unsigned col);
	private:
		int ellSize;
		int ellPos;
};

} //namespace connect_four
#endif // BOARDSCREEN_H

