
#include "NewGameWindow.h"

#include <QGridLayout>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "backend/MenschlicherSpieler.h"
#include "backend/AiSpieler.h"
#include "backend/StandardBewertungsFunktion.h"
#include "backend/Spiel.h"

namespace connect_four{


NewGameWindow::NewGameWindow(QWidget* parent, Qt::WindowFlags f)
    : QWidget(parent, f) {
    this->setWindowTitle(tr("New Game"));
    //this->setFixedSize(400, 250);
    initWindow();
}

void NewGameWindow:: initWindow() {
    QGridLayout* layout = new QGridLayout(this);
    this->setLayout(layout);
    QLabel* lb = new QLabel(tr("Breite"));
    QLabel* lh = new QLabel(tr("Hoehe"));
    QLabel* lsb = new QLabel(tr("Sieganzahl"));
    QLabel* ls = new QLabel(tr("Spiel:"));
    QComboBox* cbb = new QComboBox();
    QComboBox* cbh = new QComboBox();
    QComboBox* cbs = new QComboBox();
    for(unsigned i = 4; i < 10; i++){
      cbb->addItem(QString::number(i+3), QVariant::fromValue(i+3));
      cbh->addItem(QString::number(i+2), QVariant::fromValue(i+2));
      cbs->addItem(QString::number(i), QVariant::fromValue(i));
    }

    QLabel* l01 = new QLabel(tr("Spielertyp"));
    QLabel* l02 = new QLabel(tr("Spielername"));
    QLabel* l03 = new QLabel(tr("Schwierigkeit"));
    QLabel* l10 = new QLabel(tr("Spieler 1:"));
    QComboBox* cb11 = new QComboBox();
    cb11->addItem(tr("Mensch"), QVariant::fromValue(PlayerType::MENSCH));
    cb11->addItem(tr("AI"), QVariant::fromValue(PlayerType::AI));
    QLineEdit* le12 = new QLineEdit(tr("RedPlayer"));
    QComboBox* cb13 = new QComboBox();
    cb13->addItem(tr("Leicht"), QVariant::fromValue(Difficulty::LEICHT));
    cb13->addItem(tr("Mittel"), QVariant::fromValue(Difficulty::MITTEL));
    cb13->addItem(tr("Schwer"), QVariant::fromValue(Difficulty::SCHWER));
    cb13->setEnabled(false);
    QObject::connect( cb11 ,
                      static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), cb13,
                      &QComboBox::setEnabled);
    QLabel* l20 = new QLabel(tr("Spieler 2:"));
    QComboBox* cb21 = new QComboBox();
    cb21->addItem(tr("AI"), QVariant::fromValue(PlayerType::AI));
    cb21->addItem(tr("Mensch"), QVariant::fromValue(PlayerType::MENSCH));
    QLineEdit* le22 = new QLineEdit(tr("YellowPlayer"));
    QComboBox* cb23 = new QComboBox();
    cb23->addItem(tr("Leicht"), QVariant::fromValue(Difficulty::LEICHT));
    cb23->addItem(tr("Mittel"), QVariant::fromValue(Difficulty::MITTEL));
    cb23->addItem(tr("Schwer"), QVariant::fromValue(Difficulty::SCHWER));
    QObject::connect( cb21 ,
                      static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), cb23,
                      &QComboBox::setDisabled);
    QPushButton* pb30 = new QPushButton(tr("Spiel erstellen"));
    QObject::connect( pb30 , &QPushButton::clicked, this,
                      &NewGameWindow::createGame);
    layout->addWidget(lb, 0, 1, Qt::AlignTop);
    layout->addWidget(lh, 0, 2, Qt::AlignTop);
    layout->addWidget(lsb, 0, 3, Qt::AlignTop);
    layout->addWidget(ls, 1, 0, Qt::AlignTop);
    layout->addWidget(cbb, 1, 1, Qt::AlignTop);
    layout->addWidget(cbh, 1, 2, Qt::AlignTop);
    layout->addWidget(cbs, 1, 3, Qt::AlignTop);

    layout->addWidget(l01, 2, 1, Qt::AlignTop);
    layout->addWidget(l02, 2, 2, Qt::AlignTop);
    layout->addWidget(l03, 2, 3, Qt::AlignTop);
    layout->addWidget(l10, 3, 0, Qt::AlignTop);
    layout->addWidget(cb11, 3, 1, Qt::AlignTop);
    layout->addWidget(le12, 3, 2, Qt::AlignTop);
    layout->addWidget(cb13, 3, 3, Qt::AlignTop);
    layout->addWidget(l20, 4, 0, Qt::AlignTop);
    layout->addWidget(cb21, 4, 1, Qt::AlignTop);
    layout->addWidget(le22, 4, 2, Qt::AlignTop);
    layout->addWidget(cb23, 4, 3, Qt::AlignTop);
    layout->addWidget(pb30, 5, 0, Qt::AlignBottom);
}


void NewGameWindow:: createGame() {
    Spieler* sp1, *sp2;
    QGridLayout* layout = qobject_cast<QGridLayout*>(this->layout());
    for (int i = 3; i <= 4; i++) {
        Spieler** temp = (i == 3) ? &sp1 : &sp2;
        QLineEdit* le = qobject_cast<QLineEdit*>(layout->itemAtPosition(i,
                        2)->widget());
        std::string spName (le->text().toStdString());
        QComboBox* cb1 = qobject_cast<QComboBox*>(layout->itemAtPosition(i,
                         1)->widget());
        PlayerType pt = cb1->currentData().value<PlayerType>();
        if (pt == PlayerType::AI) {
            QComboBox* cb3 = qobject_cast<QComboBox*>(layout->itemAtPosition(i,
                             3)->widget());
            Difficulty dc = cb3->currentData().value<Difficulty>();
            (*temp) = new AiSpieler(spName,
                                    (i == 3) ? FeldStatus::SPIELER1 : FeldStatus::SPIELER2,
                                    std::unique_ptr<BewertungsFunktion>(
                                        new StandardBewertungsFunktion()), dc);
		}
		else {
			(*temp) = new MenschlicherSpieler(spName,
				((i == 3) ? FeldStatus::SPIELER1 : FeldStatus::SPIELER2));
		}
    }

    QComboBox* cbb = qobject_cast<QComboBox*>(layout->itemAtPosition(1,
                         1)->widget());
    unsigned width = cbb->currentData().value<unsigned>();
    QComboBox* cbh = qobject_cast<QComboBox*>(layout->itemAtPosition(1,
                         2)->widget());
    unsigned height = cbh->currentData().value<unsigned>();
    QComboBox* cbs = qobject_cast<QComboBox*>(layout->itemAtPosition(1,
                         3)->widget());
    unsigned limit = cbs->currentData().value<unsigned>();
    Spiel* game = new Spiel(sp1, sp2, width, height, limit);
    emit gameCreated(game);
    this->close();
}

} //namespace connect_four

