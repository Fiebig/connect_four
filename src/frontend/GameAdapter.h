#ifndef GAME_ADAPTER_H
#define GAME_ADAPTER_H

#include <memory>
#include <atomic>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include "backend/Spiel.h"

namespace connect_four{


/**
 ******************************************
 * @class GameAdapter
 * The GameAdapter class acts as an interface
 * between model and view.
 ******************************************
 */
class GameAdapter : public QThread {
        Q_OBJECT
        std::unique_ptr<Spiel> game;
        std::atomic_bool m_interrupt;
        QMutex mutexGame;
        QWaitCondition nextAiMove;
    public:        
        explicit GameAdapter(Spiel* s);
        ~GameAdapter();
        const Spielbrett& getSb();
        const Spieler& getCurrentPlayer();
        bool isGameFinished();
        void humanMove(unsigned col);
    signals:
        void boardChanged(const Spielbrett& sb);
        void gameFinished(const QString& msg);
    private:
        void run() override;
        void nextPlayerMove(unsigned col);

};

} //namespace connect_four
#endif // GAME_ADAPTER_H

