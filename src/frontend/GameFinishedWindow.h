#ifndef GAMEFINISHEDWINDOW_H
#define GAMEFINISHEDWINDOW_H

#include <QWidget>

namespace connect_four{


class GameFinishedWindow : public QWidget {
        Q_OBJECT
    public:
        GameFinishedWindow(const QString& message, QWidget* parent = 0,
                           Qt::WindowFlags f = 0);
    signals:
        void restartGameRequest();
        void newGameRequest();
};


} //namespace connect_four
#endif // GAMEFINISHEDWINDOW_H

