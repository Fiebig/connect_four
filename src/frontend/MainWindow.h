#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QResizeEvent;

namespace connect_four{


class GameAdapter;
class Spiel;

class MainWindow : public QMainWindow {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = 0);
        ~MainWindow();
    public slots:
        void initGame(Spiel* game);
    private slots:
        void executeMove(unsigned col);
        void openNewGameWindow();
        void openGameFinishedWindow(const QString& msg);
    private:
        void createMenus();
        void createStatusBar();
		void resizeEvent(QResizeEvent *event) override;
        GameAdapter* gameAdapter;
};

} //namespace connect_four

#endif // MAINWINDOW_H
