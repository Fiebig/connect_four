#include <cstdlib>
#include <ctime>
#include <QApplication>

#include "frontend/MainWindow.h"
#include "backend/MenschlicherSpieler.h"
#include "backend/AiSpieler.h"
#include "backend/StandardBewertungsFunktion.h"
#include "backend/Spiel.h"
#include "backend/MinMaxAI.h"


static connect_four::Spiel* createInitialGame() {
    using namespace connect_four;
    Spieler* sp1 = new MenschlicherSpieler("RedPlayer", FeldStatus::SPIELER1);
    Spieler* sp2 = new AiSpieler("YellowPlayer", FeldStatus::SPIELER2,
                                 std::unique_ptr<BewertungsFunktion>(
                                     new StandardBewertungsFunktion()), Difficulty::LEICHT);
    Spiel* game = new Spiel(sp1, sp2);
    return game;
}


int main(int argc, char** argv) {
	srand(time(NULL));
    if(argc > 1) connect_four::MinMaxAI::threadCount = atoi(argv[1]);
    QApplication a(argc, argv);
    connect_four::MainWindow w;
    w.setMinimumSize(600, 480);
    //set on default
    w.setAttribute(Qt::WA_QuitOnClose, true);
    w.initGame(createInitialGame());
    w.show();
    return a.exec();
}
